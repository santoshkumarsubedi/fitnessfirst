package com.example.finalassignment.home.ui.slideshow.adapter;

public class ProgressDataModel {
    private int id;
    private String date;
    private String weight;
    private String remark;
    private String imageName;

    public ProgressDataModel(String date, String weight, String remark, String imageName,int id) {
        this.date = date;
        this.id=id;
        this.weight = weight;
        this.remark = remark;
        this.imageName = imageName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }
}
