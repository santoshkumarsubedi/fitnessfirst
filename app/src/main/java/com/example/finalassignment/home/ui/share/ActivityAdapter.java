package com.example.finalassignment.home.ui.share;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.finalassignment.R;
import com.example.finalassignment.utils.Global;
import com.example.finalassignment.utils.response.ActivityResponse;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;

public class ActivityAdapter extends RecyclerView.Adapter<ActivityAdapter.ActivityViewHolder> {

    ArrayList<ActivityResponse> data;
    Context context;

    public ActivityAdapter(ArrayList<ActivityResponse> data, Context context){
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public ActivityViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_diet_and_exerciese,parent,false);
        return new ActivityViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ActivityViewHolder holder, int position) {
        ActivityResponse response = data.get(position);
        holder.textView.setText(response.getTitle());
        Picasso.with(context).load(Global.baseUrl+"progressimage.php?name="+response.getCover_photo()).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                holder.linearLayout.setBackground(new BitmapDrawable(context.getResources(),bitmap));
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(ArrayList<ActivityResponse> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public class ActivityViewHolder extends RecyclerView.ViewHolder{
        TextView textView;
        RelativeLayout linearLayout;
        public ActivityViewHolder(@NonNull View itemView) {
            super(itemView);
            linearLayout = itemView.findViewById(R.id.diet_and_exercices_slideshow_layout);
            textView = itemView.findViewById(R.id.diet_and_exercices_slideshow_title);
        }
    }
}
