package com.example.finalassignment.home.ui.home;

import android.content.Context;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.app.progresviews.ProgressLine;
import com.app.progresviews.ProgressWheel;
import com.example.finalassignment.R;
import com.example.finalassignment.database.entity.DailySteps;
import com.example.finalassignment.database.entity.LoggedUser;
import com.example.finalassignment.database.repository.DailyStepsRepository;
import com.example.finalassignment.home.ui.share.ActivityResponseData;
import com.example.finalassignment.utils.Global;
import com.example.finalassignment.utils.pedometer.StepDetector;
import com.example.finalassignment.utils.pedometer.StepListener;
import com.example.finalassignment.utils.response.RegisterResponse;
import com.example.finalassignment.utils.response.StepsReponseDataModel;
import com.example.finalassignment.utils.response.StepsResponse;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.DefaultAxisValueFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.Utils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment implements SensorEventListener, StepListener {
    private StepDetector stepDetector;
    private SensorManager sensorManager;
    private Sensor sensor;
    ProgressWheel wheel;
    ProgressLine progressLine;
    LineChart lineChart;
    private int steps=0;
    DailyStepsRepository repository;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_home, container, false);
        wheel = root.findViewById(R.id.wheelprogress);
        progressLine = root.findViewById(R.id.progress_line);
        wheel.setPercentage(100);

        lineChart = root.findViewById(R.id.chart1);
        lineChart.getDescription().setEnabled(true);
        lineChart.setTouchEnabled(true);
        lineChart.setPinchZoom(true);
        repository = new DailyStepsRepository(getActivity().getApplication());

        LineGraphMarkerView mv = new LineGraphMarkerView(getActivity().getApplicationContext(),R.layout.linegraph_maker_view);
        mv.setChartView(lineChart);
        lineChart.setMarker(mv);
        renderData();

        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                repository.insert(steps);
            }
        };

        Timer timer = new Timer();
        timer.schedule(task,20000L,30000L);


        sensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);

        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        stepDetector = new StepDetector();

        stepDetector.registerListener(this);

        sensorManager.registerListener(HomeFragment.this,sensor,SensorManager.SENSOR_DELAY_FASTEST);



        new GetSteps().execute();

        updateStep();

        return root;
    }

    public ArrayList<String> xAxisValue(){
        ArrayList<String> xaxis = new ArrayList<>();
        DateTime dateTime = new DateTime();
        DateTimeFormatter formatter = DateTimeFormat.forPattern("E,dd");
        for (int i=6;i>=1;i--){
            DateTime before = dateTime.minusDays(i);
            xaxis.add(formatter.print(before));
        }
        xaxis.add(formatter.print(dateTime));
        return xaxis;
    }

    public void renderData() {
        DateTime dateTime = new DateTime();
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTimeFormatter formatter1 = DateTimeFormat.forPattern("MM/dd");
        DateTime dateTime1 = dateTime.minusDays(7);
        LoggedUser loggedUser = Global.loggedUser;
        ArrayList<String> xVals = new ArrayList<>();
        Call<StepsResponse> call = Global.calls.getSteps(loggedUser.getToken(),String.valueOf(loggedUser.getId()),
                "getSteps",formatter.print(dateTime),formatter.print(dateTime1));
        call.enqueue(new Callback<StepsResponse>() {
            @Override
            public void onResponse(Call<StepsResponse> call, Response<StepsResponse> response) {
                ArrayList<StepsReponseDataModel> data = response.body().getData();
                ArrayList<Entry> values = new ArrayList<>();
                for(int i = 0;i<data.size();i++){
                    values.add(new Entry(i,data.get(i).getSteps()));
                    xVals.add(formatter1.print(formatter.parseDateTime(data.get(i).getDates())));
                }
                setData(values,xVals,data.size());
            }

            @Override
            public void onFailure(Call<StepsResponse> call, Throwable t) { }});
    }

    private void setData(ArrayList<Entry> values,ArrayList<String> xVals,int size) {
        System.out.println(xVals.size()-1);
        LimitLine llXAxis = new LimitLine(10f, "Index 10");
        llXAxis.setLineWidth(4f);
        //llXAxis.enableDashedLine(10f, 10f, 0f);
        llXAxis.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
        llXAxis.setLabel("Number of Steps");
        llXAxis.setTextSize(10f);
        XAxis xAxis = lineChart.getXAxis();
        xAxis.setAxisMaximum(size-1);
        xAxis.setAxisMinimum(0f);
        xAxis.setDrawLimitLinesBehindData(true);
        xAxis.setValueFormatter(new IndexAxisValueFormatter(){
            @Override
            public String getFormattedValue(float value) {
                return xVals.get((int) value);
            }
        });
        YAxis leftAxis = lineChart.getAxisLeft();
        leftAxis.removeAllLimitLines();
        leftAxis.setAxisMaximum(1000f);
        leftAxis.setAxisMinimum(0f);
        leftAxis.enableGridDashedLine(10f, 10f, 0f);
        leftAxis.setDrawZeroLine(false);
        leftAxis.setDrawLimitLinesBehindData(false);
        lineChart.getAxisRight().setEnabled(false);


        LineDataSet set1;
        if (lineChart.getData() != null &&
                lineChart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) lineChart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            lineChart.getData().notifyDataChanged();
            lineChart.notifyDataSetChanged();
        } else {
            set1 = new LineDataSet(values, "Last 7 Days");
            set1.setDrawIcons(false);
            set1.enableDashedLine(10f, 5f, 0f);
            set1.enableDashedHighlightLine(10f, 5f, 0f);
            set1.setColor(Color.DKGRAY);
            set1.setCircleColor(Color.RED);
            set1.setLineWidth(1f);
            set1.setCircleRadius(3f);
            set1.setDrawCircleHole(false);
            set1.setValueTextSize(9f);
            set1.setDrawFilled(true);
            set1.setFormLineWidth(1f);
            set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            set1.setFormSize(15.f);

            if (Utils.getSDKInt() >= 18) {
                //Drawable drawable = ContextCompat.getDrawable(this, R.drawable.);
                //set1.setFillDrawable(drawable);
            } else {
                set1.setFillColor(Color.DKGRAY);
            }
            ArrayList<ILineDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);
            LineData data = new LineData(dataSets);
            lineChart.setData(data);
        }
    }



        @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            stepDetector.updateAccel(
                    event.timestamp, event.values[0], event.values[1], event.values[2]);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void step(long timeNs) {
        steps++;
        wheel.setStepCountText(String.valueOf(steps));
        progressLine.setmPercentage((int) (steps));
        progressLine.setmValueText(steps);
        wheel.setPercentage((int) (steps*3.6));
    }


    public void updateStep(){
        LocalDate date = new LocalDate().minusDays(1);
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        String stringDate = formatter.print(date);
        try {
            new UpdateSteps().execute(stringDate);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    class UpdateSteps extends AsyncTask<String,Void,Void> {
        DailySteps steps;
        @Override
        protected Void doInBackground(String... dates) {
            steps = repository.getSteps(dates[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (steps != null) {
                if (!steps.isUpdated()) {
                    LoggedUser loggedUser = Global.loggedUser;
                    Call<RegisterResponse> call = Global.calls.postSteps(loggedUser.getToken(),
                            String.valueOf(loggedUser.getUserId()), "uploadSteps", steps.getDate().toString(),
                            steps.getSteps());
                    call.enqueue(new Callback<RegisterResponse>() {
                        @Override
                        public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                            if (response.body().getStatus().equals("success")) {
                                steps.setUpdated(true);
                                repository.update(steps);
                            }
                        }
                        @Override
                        public void onFailure(Call<RegisterResponse> call, Throwable t) {
                        }
                    });
                }
            }
        }
    }



    class GetSteps extends AsyncTask<Void,Void,Void>{
        DailySteps step;
        @Override
        protected Void doInBackground(Void... voids) {
            step = repository.getSteps(Global.datestring);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(step!=null) {
                steps = step.getSteps();
                wheel.setStepCountText(String.valueOf(steps));
                progressLine.setmPercentage((int) (steps));
                progressLine.setmValueText(steps);
                wheel.setPercentage((int) (steps * 3.6));
            }else{
                System.out.println("step is null");
                System.out.println(Global.getDate());
            }
        }
    }
}