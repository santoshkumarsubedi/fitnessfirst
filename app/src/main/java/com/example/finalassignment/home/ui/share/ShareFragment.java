package com.example.finalassignment.home.ui.share;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.finalassignment.R;
import com.example.finalassignment.database.entity.LoggedUser;
import com.example.finalassignment.utils.Global;
import com.example.finalassignment.utils.response.ActivityResponse;
import com.example.finalassignment.utils.response.SlideShowResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShareFragment extends Fragment {

    private RecyclerView recyclerView;
    ActivityAdapter adapter;
    ArrayList<ActivityResponse> data;
    LoggedUser loggedUser;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        data = new ArrayList<>();
        adapter = new ActivityAdapter(data,getContext());
        View view = inflater.inflate(R.layout.fragment_share, container, false);
        recyclerView = view.findViewById(R.id.diet_and_exercices_slideshow_recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);
        Call<ActivityResponseData> call = Global.calls.getActivityData(Global.loggedUser.getToken(),"getActivity");
        call.enqueue(new Callback<ActivityResponseData>() {
            @Override
            public void onResponse(Call<ActivityResponseData> call, Response<ActivityResponseData> response) {
                adapter.setData(response.body().getData());
                System.out.println(response.body().getStatus());
            }

            @Override
            public void onFailure(Call<ActivityResponseData> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });
        return view;
    }
}