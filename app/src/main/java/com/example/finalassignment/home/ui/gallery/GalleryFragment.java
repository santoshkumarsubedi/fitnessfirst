package com.example.finalassignment.home.ui.gallery;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.finalassignment.R;
import com.example.finalassignment.database.entity.LoggedUser;
import com.example.finalassignment.utils.ApiCalls;
import com.example.finalassignment.utils.Global;
import com.example.finalassignment.utils.response.RegisterResponse;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class GalleryFragment extends Fragment {

    private GalleryViewModel galleryViewModel;
    TextInputEditText dateEditText,remarkEditText;
    MaterialButton click,upload;
    ImageView imageView;
    NumberPicker weightPicker;
    File file;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private static final int CAMERA_REQUEST = 1888;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        galleryViewModel =
                ViewModelProviders.of(this).get(GalleryViewModel.class);
        View view = inflater.inflate(R.layout.fragment_gallery, container, false);
        dateEditText = view.findViewById(R.id.progress_date_textfield);
        remarkEditText = view.findViewById(R.id.progress_remark_textfield);
        click = view.findViewById(R.id.progress_click_button);
        imageView = view.findViewById(R.id.set_image);
        weightPicker = view.findViewById(R.id.progress_spinner_weight);
        upload = view.findViewById(R.id.progress_upload_button);
        weightPicker.setMinValue(20);
        weightPicker.setMaxValue(200);
        weightPicker.setValue(60);

        click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(getActivity(),Manifest.permission.CAMERA) !=
                        PackageManager.PERMISSION_GRANTED)
                {
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                }
                else
                {
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, CAMERA_REQUEST);
                }
            }
        });

        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isRemarkValid(remarkEditText.getText())) {
                    if(file!=null) {
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), file);
                        MultipartBody.Part fileUpload = MultipartBody.Part.createFormData("file",
                                                                        file.getName(), requestBody);
                        RequestBody username = RequestBody.create(MediaType.parse("text/plain"), "1");
                        RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());
                        RequestBody date = RequestBody.create(MediaType.parse("text/plain"), simpleDateFormat.format(new Date()));
                        RequestBody weight = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(weightPicker.getValue()));
                        RequestBody remark = RequestBody.create(MediaType.parse("text/plain"), remarkEditText.getText().toString());
                        RequestBody status = RequestBody.create(MediaType.parse("text/plain"), "uploadProgress");
                        Call<RegisterResponse> call = Global.calls.postProgress(Global.loggedUser.getToken(),
                                fileUpload, filename, username, date, remark, weight, status);

                        call.enqueue(new Callback<RegisterResponse>() {
                            @Override
                            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                                if (response.isSuccessful()) {
                                    Toast.makeText(getActivity().getApplicationContext(), response.body().getStatus(), Toast.LENGTH_LONG).show();
                                }
                            }
                            @Override
                            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                            }
                        });
                    }else{
                        Toast.makeText(getActivity(), "Please Click an image", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    remarkEditText.setError("Give some feedback");
                }
            }
        });
        DateTime dateTime = new DateTime();
        DateTimeFormatter formatter = DateTimeFormat.forPattern("E,dd");
        dateEditText.setText(formatter.print(dateTime));
        dateEditText.setEnabled(false);
        return view;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_PERMISSION_CODE)
        {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                Toast.makeText(getContext(), "camera permission granted", Toast.LENGTH_LONG).show();
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
            else
            {
                Toast.makeText(getContext(), "camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    private boolean isRemarkValid(@Nullable Editable text){
        return text!=null&&text.length()>=8;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK)
            {
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                imageView.setImageBitmap(photo);
                try{
                file = new File(getContext().getCacheDir(), "temp.jpeg");
                file.createNewFile();
                OutputStream fos = new BufferedOutputStream(new FileOutputStream(file));
                photo.compress(Bitmap.CompressFormat.JPEG,100,fos);
                fos.close();
            }catch(Exception ex){}
            }
    }

}