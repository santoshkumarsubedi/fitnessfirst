package com.example.finalassignment.home.ui.tools;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.finalassignment.R;
import com.example.finalassignment.database.entity.LoggedUser;

public class ToolsFragment extends Fragment {

    private ToolsViewModel toolsViewModel;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        toolsViewModel =
                ViewModelProviders.of(this).get(ToolsViewModel.class);
        View view = inflater.inflate(R.layout.fragment_tools, container, false);
        TextView poweredBy = view.findViewById(R.id.poweredBy);
        poweredBy.setText(Html.fromHtml("<b><font face='cursive'> Powered By CodePoets</font></b><br/><i><font face='cursive' size='20'><b>make IT work</b></font></i>"));

        return view;
    }
}