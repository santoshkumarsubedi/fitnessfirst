package com.example.finalassignment.home.ui.share;

import com.example.finalassignment.home.ui.slideshow.adapter.ProgressDataModel;
import com.example.finalassignment.utils.response.ActivityResponse;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class ActivityResponseData implements Serializable {
    @SerializedName("Status")
    private String status;

    @SerializedName("Data")
    private ArrayList<ActivityResponse> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<ActivityResponse> getData() {
        return data;
    }

    public void setData(ArrayList<ActivityResponse> data) {
        this.data = data;
    }
}
