package com.example.finalassignment.home.ui.slideshow;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.finalassignment.R;
import com.example.finalassignment.database.entity.LoggedUser;
import com.example.finalassignment.database.repository.LoggedUserRepository;
import com.example.finalassignment.home.ui.slideshow.adapter.ProgressDataModel;
import com.example.finalassignment.home.ui.slideshow.adapter.SlideshowAdapter;
import com.example.finalassignment.utils.Global;
import com.example.finalassignment.utils.response.RegisterResponse;
import com.example.finalassignment.utils.response.SlideShowResponse;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SlideshowFragment extends Fragment {
    RecyclerView recyclerView;
    SlideshowAdapter slideshowAdapter;
    ArrayList<ProgressDataModel> dataModels;
    LoggedUserRepository loggedUserRepository;
    LoggedUser loggedUser;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_slideshow, container, false);
        recyclerView = view.findViewById(R.id.slideshow_recyclerview);
        dataModels = new ArrayList<>();
        slideshowAdapter = new SlideshowAdapter(dataModels,getContext());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(slideshowAdapter);
        loggedUserRepository = new LoggedUserRepository(getActivity().getApplication());

        Call<SlideShowResponse> call = Global.calls.getProgressData(Global.loggedUser.getToken(),
                String.valueOf(Global.loggedUser.getUserId()),"getProgress");
        call.enqueue(new Callback<SlideShowResponse>() {
            @Override
            public void onResponse(Call<SlideShowResponse> call, Response<SlideShowResponse> response) {
                if(response.body().getStatus().equals("success")) {
                    slideshowAdapter.setData(response.body().getData());
                }
            }

            @Override
            public void onFailure(Call<SlideShowResponse> call, Throwable t) {
                System.out.println("failed"+t.getMessage());
            }
        });

        new GetLoggedUser().execute();

        return view;
    }

    public class GetLoggedUser extends AsyncTask<Void,Void,Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            loggedUser = loggedUserRepository.getUser();
            return null;

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Call<SlideShowResponse> call = Global.calls.getProgressData(loggedUser.getToken(),String.valueOf(loggedUser.getUserId()),"getProgress");
            call.enqueue(new Callback<SlideShowResponse>() {
                @Override
                public void onResponse(Call<SlideShowResponse> call, Response<SlideShowResponse> response) {

                    //System.out.println("success"+response.body().getData());
                    if(response.body().getStatus().equals("success")) {
                        slideshowAdapter.setData(response.body().getData());
                    }
                }

                @Override
                public void onFailure(Call<SlideShowResponse> call, Throwable t) {
                    System.out.println("failed"+t.getMessage());
                }
            });
        }
    }
}