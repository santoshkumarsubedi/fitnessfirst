package com.example.finalassignment.home.ui.slideshow.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.finalassignment.R;
import com.example.finalassignment.utils.Global;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class SlideshowAdapter extends RecyclerView.Adapter<SlideshowAdapter.SlideShowViewHolder> {

    private ArrayList<ProgressDataModel> data;
    Context context;

    public SlideshowAdapter(ArrayList<ProgressDataModel> data, Context context){
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public SlideShowViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_slideshow_progress
                ,parent,false);
        return new SlideShowViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SlideShowViewHolder holder, int position) {
        ProgressDataModel model = data.get(position);
        holder.date.setText(model.getDate());
        holder.weight.setText(model.getWeight());
        holder.remark.setText(model.getRemark());
        Picasso.with(context).load(Global.baseUrl+"progressimage.php?name="+model.getImageName()).into(holder.image);
        System.out.println(model.getImageName());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(ArrayList<ProgressDataModel> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public class SlideShowViewHolder extends RecyclerView.ViewHolder{
        TextView date;
        TextView weight;
        TextView remark;
        ImageView image;

        public SlideShowViewHolder(@NonNull View view) {
            super(view);
            date = view.findViewById(R.id.entry_date);
            weight = view.findViewById(R.id.entry_weight);
            remark = view.findViewById(R.id.entry_remark);
            image = view.findViewById(R.id.entry_image_view);
        }
    }
}
