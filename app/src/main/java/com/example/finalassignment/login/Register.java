package com.example.finalassignment.login;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.finalassignment.R;
import com.example.finalassignment.interfaces.NavigationHost;
import com.example.finalassignment.utils.Global;
import com.example.finalassignment.utils.RequestPOJO.RegisterRequest;
import com.example.finalassignment.utils.response.RegisterResponse;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Register extends Fragment {
    private TextInputEditText eFullName,eEmail,eAddress,eUsername,ePassword;
    private TextInputLayout lFullName,lEmail,lAddress,lUsername,lPassword;
    private MaterialButton bNext,bCancel;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_register, container, false);
        bCancel = view.findViewById(R.id.register_cancel_button);
        bNext = view.findViewById(R.id.register_next_button);
        eFullName = view.findViewById(R.id.register_fullname_textfield);
        lFullName = view.findViewById(R.id.register_fullname_inputlayout);
        eEmail = view.findViewById(R.id.register_email_textfield);
        lEmail = view.findViewById(R.id.register_email_inputlayout);
        eAddress = view.findViewById(R.id.register_address_textfield);
        lAddress = view.findViewById(R.id.register_address_inputlayout);
        eUsername = view.findViewById(R.id.register_username_textfield);
        lUsername = view.findViewById(R.id.register_username_inputlayout);
        ePassword = view.findViewById(R.id.register_password_textfield);
        lPassword = view.findViewById(R.id.register_password_inputlayout);

        bNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fullname = eFullName.getText().toString();
                String email = eEmail.getText().toString();
                String address = eAddress.getText().toString();
                String username = eUsername.getText().toString();
                String password = ePassword.getText().toString();
                if(isUsernameValid(username)) {
                    Call<RegisterResponse> call = Global.calls.postRegister(fullname, email, address, username, password, "register");
                    call.enqueue(new Callback<RegisterResponse>() {
                        @Override
                        public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                            if (response.isSuccessful()) {
                                System.out.println("status" + response.body().getStatus());
                                ((NavigationHost) getActivity()).navigateTo(new Register(), true);
                            }
                        }

                        @Override
                        public void onFailure(Call<RegisterResponse> call, Throwable t) {
                            Toast.makeText(getActivity().getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println(t.fillInStackTrace());
                        }
                    });
                }else{
                    lUsername.setError("Username cannot be null");
                }
            }
        });

        bCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((NavigationHost) getActivity()).navigateTo(new Register(), true);
            }
        });
        return view;
    }


    private boolean isUsernameValid(@Nullable String text){
        return text!=null&&text.length()>=5;
    }
}
