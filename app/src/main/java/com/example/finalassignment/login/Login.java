package com.example.finalassignment.login;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.finalassignment.R;
import com.example.finalassignment.database.entity.LoggedUser;
import com.example.finalassignment.database.repository.LoggedUserRepository;
import com.example.finalassignment.home.Home;
import com.example.finalassignment.interfaces.NavigationHost;
import com.example.finalassignment.utils.Global;
import com.example.finalassignment.utils.response.LoginResponse;
import com.example.finalassignment.utils.response.RegisterResponse;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends Fragment {

    private MaterialButton mBLogin,mBRegister;
    private TextInputEditText tEUsername,tEPassword;
    private TextInputLayout tLUsername,tLPassword;
    LoggedUserRepository loggedUserRepository;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_login, container, false);
        mBLogin = view.findViewById(R.id.login_next_button);
        mBRegister = view.findViewById(R.id.login_Register_button);
        tEUsername = view.findViewById(R.id.login_username_textfield);
        tEPassword = view.findViewById(R.id.login_password_textfield);
        tLUsername = view.findViewById(R.id.login_username_inputlayout);
        tLPassword = view.findViewById(R.id.login_password_inputlayout);
        loggedUserRepository = new LoggedUserRepository(getActivity().getApplication());

        mBLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isPasswordValid(tEPassword.getText())){
                    tLPassword.setError("Password mush be >8 characters");
                }else if(!isUsernameValid(tEUsername.getText())){
                    tLUsername.setError("Username must be >5 characters");
                }else{
                    tLPassword.setError(null);
                    tLUsername.setError(null);
                    String password = tEPassword.getText().toString();
                    String username = tEUsername.getText().toString();

                    Call<LoginResponse> call = Global.calls.postLogin(username,password,"login");
                    call.enqueue(new Callback<LoginResponse>() {
                        @Override
                        public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                            if(response.body().getStatus().equals("success")){
                                Toast.makeText(getActivity().getApplicationContext(), "success", Toast.LENGTH_SHORT).show();
                                LoginResponse res = response.body();
                                LoggedUser loggedUser = new LoggedUser();
                                loggedUser.setId(1);
                                loggedUser.setFullName(res.getFullname());
                                loggedUser.setToken(res.getSession_id());
                                loggedUser.setUserId(res.getId());
                                loggedUser.setUsername(res.getUsername());
                                loggedUserRepository.insert(loggedUser);
                                Global.loggedUser = loggedUser;
                                Intent intent  = new Intent(getActivity(), Home.class);
                                startActivity(intent);
                            }else if(response.body().getStatus().equals("failed")) {
                                Toast.makeText(getActivity().getApplicationContext(), "failed", Toast.LENGTH_SHORT).show();
                            }
                        }
                        @Override
                        public void onFailure(Call<LoginResponse> call, Throwable t) {
                            System.out.println(t.getMessage());
                            Toast.makeText(getActivity().getApplicationContext(), "Failed", Toast.LENGTH_SHORT).show();
                        }
                    });
                }


            }
        });

        mBRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((NavigationHost) getActivity()).navigateTo(new Register(), true);
            }
        });


        return view;
    }

    private boolean isPasswordValid(@Nullable Editable text){
        return text!=null&&text.length()>=5;
    }


    private boolean isUsernameValid(@Nullable Editable text){
        return text!=null&&text.length()>=5;
    }

}
