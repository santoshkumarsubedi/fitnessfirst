package com.example.finalassignment.database.repository;

import android.app.Application;
import android.os.AsyncTask;

import com.example.finalassignment.database.AppDatabase;
import com.example.finalassignment.database.dao.DailyStepsDao;
import com.example.finalassignment.database.entity.DailySteps;
import com.example.finalassignment.utils.Global;

import java.util.Date;

public class DailyStepsRepository {
    private DailyStepsDao stepsDao;

    public DailyStepsRepository(Application application){
        AppDatabase appDatabase = AppDatabase.getAppDatabase(application);
        stepsDao = appDatabase.stepsDao();
    }

    public DailySteps getSteps(String date){
        return stepsDao.getDailySteps(date);
    }

    public void insert(int steps){
        new InsertSteps().execute(steps);
    }

    public void update(DailySteps dailySteps){
        new UpdateSteps().execute(dailySteps);
    }

    class UpdateSteps extends AsyncTask<DailySteps,Void,Void>{

        @Override
        protected Void doInBackground(DailySteps... dailySteps) {
            stepsDao.update(dailySteps[0]);
            return null;
        }
    }

    class InsertSteps extends AsyncTask<Integer,Void,Void>{

        @Override
        protected Void doInBackground(Integer... ints) {
            DailySteps steps = stepsDao.getDailySteps(Global.datestring);
            if(steps==null){
                DailySteps newdata = new DailySteps();
                newdata.setDate(Global.datestring);
                newdata.setSteps(ints[0]);
                stepsDao.insert(newdata);
            }else{
                steps.setSteps(ints[0]);
                stepsDao.update(steps);
            }
            return null;
        }
    }


}
