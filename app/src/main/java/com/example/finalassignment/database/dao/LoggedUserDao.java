package com.example.finalassignment.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.finalassignment.database.entity.LoggedUser;

@Dao
public interface LoggedUserDao {

    @Query("SELECT * FROM loggedUser where id=1")
    LoggedUser getLoggedUser();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(LoggedUser user);
}
