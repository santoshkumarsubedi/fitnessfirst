package com.example.finalassignment.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.finalassignment.database.entity.DailySteps;

import java.util.Date;
@Dao
public interface DailyStepsDao{
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(DailySteps steps);

    @Update
    void update(DailySteps steps);

    @Query("SELECT * FROM daily_steps WHERE date=:ID")
    DailySteps getDailySteps(String ID);

}
