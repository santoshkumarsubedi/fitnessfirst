package com.example.finalassignment.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.example.finalassignment.database.dao.DailyStepsDao;
import com.example.finalassignment.database.dao.LoggedUserDao;
import com.example.finalassignment.database.entity.DailySteps;
import com.example.finalassignment.database.entity.LoggedUser;
import com.example.finalassignment.utils.TypeConverter;


@Database(entities = {LoggedUser.class, DailySteps.class},exportSchema = false,version = 1)
@TypeConverters({TypeConverter.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract LoggedUserDao loggedUser();
    public abstract DailyStepsDao stepsDao();

    private static AppDatabase INSTANCE;

    public static AppDatabase getAppDatabase(final Context context){
        if(INSTANCE==null){
            synchronized (AppDatabase.class){
                if(INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),AppDatabase.class,"database").build();
                }
            }
        }
        return INSTANCE;
    }
}
