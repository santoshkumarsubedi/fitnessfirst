package com.example.finalassignment.utils.response;

import java.util.ArrayList;

public class StepsResponse {
    private String Status;

    private ArrayList<StepsReponseDataModel> Data;

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public ArrayList<StepsReponseDataModel> getData() {
        return Data;
    }

    public void setData(ArrayList<StepsReponseDataModel> data) {
        Data = data;
    }
}
