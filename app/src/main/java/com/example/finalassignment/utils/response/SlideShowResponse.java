package com.example.finalassignment.utils.response;

import com.example.finalassignment.home.ui.slideshow.adapter.ProgressDataModel;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class SlideShowResponse implements Serializable {
    @SerializedName("Status")
    private String status;

    @SerializedName("Data")
    private ArrayList<ProgressDataModel> data;

    public ArrayList<ProgressDataModel> getData() {
        return data;
    }

    public void setData(ArrayList<ProgressDataModel> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
