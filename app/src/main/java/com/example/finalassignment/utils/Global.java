package com.example.finalassignment.utils;

import android.content.SharedPreferences;

import com.example.finalassignment.database.entity.LoggedUser;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Global {
    public static LoggedUser loggedUser;
    private static Date date = new Date();
    public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    public static String datestring = dateFormat.format(date);

    public static String baseUrl = "http://192.168.100.56/mnapi/";
    private static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    public static ApiCalls calls = retrofit.create(ApiCalls.class);

    public static Date getDate(){
        Date date = null;
        try {
            date = dateFormat.parse(datestring);
        }catch (Exception ex){

        }
        return date;
    }
}
