package com.example.finalassignment.utils.pedometer;


    // Interface used to listen for any new step detected
    public interface StepListener {

        public void step(long timeNs);

    }
