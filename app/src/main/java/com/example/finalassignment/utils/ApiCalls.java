package com.example.finalassignment.utils;

import com.example.finalassignment.home.ui.share.ActivityResponseData;
import com.example.finalassignment.utils.RequestPOJO.RegisterRequest;
import com.example.finalassignment.utils.response.ActivityResponse;
import com.example.finalassignment.utils.response.LoginResponse;
import com.example.finalassignment.utils.response.RegisterResponse;
import com.example.finalassignment.utils.response.SlideShowResponse;
import com.example.finalassignment.utils.response.StepsResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiCalls {
/*
    @POST("login.php")
    Call<RegisterResponse> postRegister(@Body RegisterRequest registerRequest);
*/

    @FormUrlEncoded
    @POST("login.php")
    Call<RegisterResponse> postRegister(@Field("fullname") String fullname,
                                            @Field("email")String email,
                                            @Field("address")String address,
                                            @Field("username")String username,
                                            @Field("password")String password,
                                            @Field("status")String status);

    @FormUrlEncoded
    @POST("login.php")
    Call<LoginResponse> postLogin(@Field("username") String username,
                                  @Field("password") String password,
                                  @Field("status") String status);

    @Multipart
    @POST("progress.php")
    Call<RegisterResponse> postProgress(
            @Header("Authorization") String authKey,
            @Part MultipartBody.Part file,
            @Part("name") RequestBody name,
            @Part("username") RequestBody username,
            @Part("date") RequestBody date,
            @Part("remark") RequestBody remark,
            @Part("weight") RequestBody weight,
            @Part("status") RequestBody status);

    @FormUrlEncoded
    @POST("progress.php")
    Call<SlideShowResponse> getProgressData(@Header("Authorization") String authKey,
                                            @Field("user_id") String id,
                                            @Field("status")String status);

    @FormUrlEncoded
    @POST("progress.php")
    Call<ActivityResponseData> getActivityData(@Header("Authorization") String authKey,
                                               @Field("status")String status);

    @FormUrlEncoded
    @POST("steps.php")
    Call<RegisterResponse> postSteps(@Header("Authorization") String authKey,
                                     @Field("user_id") String id,
                                     @Field("status") String status,
                                     @Field("dates") String date,
                                     @Field("steps") int steps);

    @FormUrlEncoded
    @POST("steps.php")
    Call<StepsResponse> getSteps(@Header("Authorization") String authKey,
                                  @Field("user_id") String id,
                                  @Field("status") String status,
                                  @Field("enddate") String enddate,
                                 @Field("firstdate") String firstdate);


}
