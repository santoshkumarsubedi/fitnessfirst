package com.example.finalassignment.utils.pedometer;

    public class SensorFilter {
        //Take float array and return the sum of the array
        public static float sum(float[] array) {
            float retval = 0;
            for (int i = 0; i < array.length; i++) {
                retval += array[i];
            }
            //return sum float value
            return retval;
        }

        //Take float array and return the normalize value
        public static float norm(float[] array) {
            float retval = 0;
            //loop through the array
            for (int i = 0; i < array.length; i++) {
                retval += array[i] * array[i];
            }
            //return the square root of the calculated value
            return (float) Math.sqrt(retval);
        }


        //take two float array and perform dot product
        public static float dot(float[] a, float[] b) {
            float retval = a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
            //return dot product of the array
            return retval;
        }

    }
