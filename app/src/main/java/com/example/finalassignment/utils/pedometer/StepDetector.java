package com.example.finalassignment.utils.pedometer;

public class StepDetector {

    // size fo the acceleration Ring
    private static final int ACCELERATION_RING_SIZE = 50;

    // size fo velocity ring
    private static final int VEL_RING_SIZE = 10;

    // Threashold can be changed to according to the sensativity
    private static final float THRESHOLD = 50f;

    //delay between two steps
    private static final int DELAY = 250000000;

    //on going ring count
    private int ACCELERATION_RING_COUNTER = 0;

    //acceleration in all dimension
    private float[] ACCELERATION_RING_X = new float[ACCELERATION_RING_SIZE];
    private float[] ACCELERATION_RING_Y = new float[ACCELERATION_RING_SIZE];
    private float[] ACCELERATION_RING_Z = new float[ACCELERATION_RING_SIZE];

    //velocity ring count
    private int VEL_RING_COUNTER = 0;
    private float[] VEL_RING = new float[VEL_RING_SIZE];
    private long LAST_STEP_TIME = 0;
    private float oldVelocityEstimate = 0;

    //object for step listener
    private StepListener listener;

    //function to register listener
    public void registerListener(StepListener listener) {
        this.listener = listener;
    }


    //take the value when the accelermeter change it's location is changed
    public void updateAccel(long timeNs, float x, float y, float z) {
        float[] currentAccel = new float[3];
        currentAccel[0] = x;
        currentAccel[1] = y;
        currentAccel[2] = z;

        // First step is recorded for estimate z value
        ACCELERATION_RING_COUNTER++;
        ACCELERATION_RING_X[ACCELERATION_RING_COUNTER % ACCELERATION_RING_SIZE] = currentAccel[0];
        ACCELERATION_RING_Y[ACCELERATION_RING_COUNTER % ACCELERATION_RING_SIZE] = currentAccel[1];
        ACCELERATION_RING_Z[ACCELERATION_RING_COUNTER % ACCELERATION_RING_SIZE] = currentAccel[2];

        // calculate sum value of all three dimension
        float[] worldZ = new float[3];
        worldZ[0] = SensorFilter.sum(ACCELERATION_RING_X) / Math.min(ACCELERATION_RING_COUNTER,
                ACCELERATION_RING_SIZE);
        worldZ[1] = SensorFilter.sum(ACCELERATION_RING_Y) / Math.min(ACCELERATION_RING_COUNTER,
                ACCELERATION_RING_SIZE);
        worldZ[2] = SensorFilter.sum(ACCELERATION_RING_Z) / Math.min(ACCELERATION_RING_COUNTER,
                ACCELERATION_RING_SIZE);

        //normalize the data of all three dimension
        float normalization_factor = SensorFilter.norm(worldZ);

        worldZ[0] = worldZ[0] / normalization_factor;
        worldZ[1] = worldZ[1] / normalization_factor;
        worldZ[2] = worldZ[2] / normalization_factor;
        //perform dot product of the arrays of data
        float currentZ = SensorFilter.dot(worldZ, currentAccel) - normalization_factor;

        //increase the vel ring counter by 1
        VEL_RING_COUNTER++;

        //add value to velocity counter array
        VEL_RING[VEL_RING_COUNTER % VEL_RING_SIZE] = currentZ;

        //filter the data from vel ring
        float velocityEstimate = SensorFilter.sum(VEL_RING);

        //check thresholad and delay to valiate the step
        if (velocityEstimate > THRESHOLD && oldVelocityEstimate <= THRESHOLD
                && (timeNs - LAST_STEP_TIME > DELAY)) {
            //step is validated and notified to the activity through interface
            listener.step(timeNs);
            //time for last step is recorded
            LAST_STEP_TIME = timeNs;
        }
        oldVelocityEstimate = velocityEstimate;
    }
}
